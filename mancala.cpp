/*	Copyright (c) 2015 Robert Towell - All rights reserved	*/

#include <iostream>
#include <sstream>
#include <string>
#include <stdlib.h>
#include <assert.h>
#include <iomanip>
using namespace std;

stringstream debug;

class Board{
private:
	int playerTurn;
	int pockets[14];
	string win;
	bool extra;
	string link[10];
	
	int correct(int n) const;
	int other(int player) const;
	int last(int pocket) const;
	int playerSide(int pocket) const;
	int across(int pocket, bool noContent) const;
	void endClear();
	void check() const;
public:
	string getLink(int n);
	bool getExtra() const;
	string getWin() const;
	int setPlayer(int player);
	int getValue(int player) const;
	int getPlayer() const;
	int getContent(int pocket, string location) const;
	int getScore(int player) const;
	string getUrl() const;
	void reset();
	bool isEnd();
	int whoWins();
	void unpause(string URL);
	void move(int choice, string location);
	void generateLinks(int turn, string theme);
	void operator=(const Board &other);
	void draw(string theme, int turn);
	Board();
	Board(const Board &other);
};
bool Board::getExtra() const{
	return extra;
}
string Board::getWin() const{
	return win;
}
int Board::getValue(int player) const{
	int value = 0;
	value += pockets[7*(player+1)-1] - pockets[7*(other(player)+1)-1];
	for(int i = 7*player; i < 7*(player+1)-1; i++){
		if(getContent(i, "getvalue") >= 1)
			if(player == 0)
				value += 6-i;
			else
				value += 13-i;
		else
			value += getContent(i, "getvalue");
	}
	return value;
}
int Board::getPlayer() const{
	return playerTurn;
}
int Board::getScore(int player) const{
	return pockets[7*(player+1)-1];
}
string Board::getUrl() const{
	stringstream url;
	url << '/' << getPlayer()
		<< '/' << pockets[0] << '/' << pockets[1] << '/' << pockets[2] << '/' << pockets[3] << '/' << pockets[4]
		<< '/' << pockets[5] << '/' << pockets[6] << '/' << pockets[7] << '/' << pockets[8] << '/' << pockets[9]
		<< '/' << pockets[10] << '/' << pockets[11] << '/' << pockets[12] << '/' << pockets[13];
	return url.str();
}
int Board::correct(int n) const{
	assert(n > 0);
	if(n < 14)
		return n;
	else if(n < 28)
		return n-14;
	else if(n < 34)
		return n-28;
	else if(n < 48)
		return n-34;
	else
		return n-48;
}
int Board::getContent(int n, string location) const{
	assert(n > -1 && n < 14);
	return pockets[n];
}
int Board::other(int player) const{
	return 1-player;
}
int Board::last(int pocket) const{	
	return pocket + getContent(pocket, "last");
}
int Board::playerSide(int pocket) const{
	if(pocket > 7*getPlayer()-1 && pocket < 7*(getPlayer()+1)-1)
		return playerTurn;
	return other(playerTurn);
}
int Board::across(int pocket, bool noContent) const{
	if(noContent == true)
		return getPlayer()*12 - getPlayer()*pocket + other(getPlayer())*12 - other(getPlayer())*pocket;
	assert(last(pocket) > 0);
	return getPlayer()*12 - getPlayer()*correct(last(pocket)) + other(getPlayer())*12 - other(getPlayer())*correct(last(pocket));
}
void Board::check() const{
	int sum = 0;
	for(int i = 0; i < 14; i++){
		sum += getContent(i, "check");
		assert(getContent(i, "check_assert ") > -1);
	}	
	assert(sum == 48);
	assert(playerTurn == 0 || playerTurn == 1);
}
bool Board::isEnd(){
	check();
	
	bool top = true;
	bool bot = true;
	for(int i = 0; i < 6; i++){
		if(pockets[i] != 0)
			top = false;
		if(pockets[i+7] != 0)
			bot = false;
	}
	if(top == true || bot == true)
		return true;
	else
		return false;
}
void Board::endClear(){
	for(int i = 0; i < 6; i++){
		pockets[6] += pockets[i];
		pockets[13] += pockets[i+7];
		pockets[i] = 0;
		pockets[i+7] = 0;
	}
}
int Board::whoWins(){
	int winner;
	if(pockets[6] > pockets[13]){
		win = "Player 1 wins";
		winner = 0;
	}
	else if(pockets[6] < pockets[13]){
		win = "Player 2 wins";
		winner = 1;
	}
	else{
		win = "Tie";
		winner = 0;
	}
	return winner;
}
void Board::unpause(string URL){
	check();
	
	for(int i = 0; i < URL.length(); i++)
		if(URL[i] == '/')
			URL[i] = ' ';
	string dummy;
	stringstream ssURL(URL);
	ssURL >> dummy >> dummy >> dummy >> dummy >> playerTurn;
	debug << "dummy: " << dummy << " ";
	for(int i = 0; i < 14; i++)
		ssURL >> pockets[i];
	
	check();
}
void Board::move(int choice, string location){
	check();
//	cout << location << choice;
	assert(choice > -1 && choice < 14);
	assert(getContent(choice, "move") > 0);
//	//simple
	int skips = 0;
	int choiceContent = getContent(choice, "move");
	pockets[choice] = 0;
	for(int i = choice+1; i < choice + choiceContent + skips +1; i++){
		if(correct(i) != (7*(other(playerTurn)+1))-1)
			pockets[correct(i)]++;
		else
			skips++;
	}
//	//check if player has an extra turn
	if(correct(choice + choiceContent) == 7*(playerTurn+1)-1)
		extra = true;
//	//check if player has a capture
	else if(playerSide(correct(choice + choiceContent)) == playerTurn
	&& pockets[choice + choiceContent] == 1
	&& pockets[across(correct(choice + choiceContent), true)] > 0){
		pockets[7*(getPlayer()+1)-1] += pockets[correct(choice + choiceContent)] + pockets[across(correct(choice + choiceContent), true)];
		pockets[correct(choice + choiceContent)] = 0;
		pockets[across(correct(choice + choiceContent), true)] = 0;
	}
//	//	
	bool t = isEnd();
	if(t)
		endClear();
	if(extra != true)
		playerTurn = other(getPlayer());
	check();	
}
void Board::operator=(const Board &other){
	playerTurn = other.playerTurn;
	extra = other.extra;
	win = other.win;
	for(int i = 0; i < 14; i++)
		pockets[i] = other.pockets[i];
	
	check();
}
void Board::reset(){
	playerTurn = 0;
	extra = false;
	win = " ";
	for(int i = 0; i < 6; i++){
		pockets[i] = 4;
		pockets[i+7] = 4;
	}
	pockets[6] = 0;
	pockets[13] = 0;
	
	check();
}
Board::Board(){
	playerTurn = 0;
	extra = false;
	win = " ";
	for(int i = 0; i < 6; i++){
		pockets[i] = 4;
		pockets[i+7] = 4;
	}
	pockets[6] = 0;
	pockets[13] = 0;
	
	check();
}
Board::Board(const Board &other){
	playerTurn = other.playerTurn;
	extra = other.extra;
	win = other.win;
	for(int i = 0; i < 14; i++)
		pockets[i] = other.pockets[i];
		
	check();
}
string Board::getLink(int n){
	return link[n];
}
void Algorithm(int p, const Board &game, int depth, int &choice, int &points);//declare Algorithm ahead of time so it can be called in generateLinks
void Board::generateLinks(int turn, string theme){
	for(int i = 7*playerTurn; i < 7*(playerTurn+1)-1; i++){
		int p = getPlayer();
		Board fake = (*this);
		if(fake.getContent(i, "generate LINKS") > 0){
			fake.move(i, "generateLinks");
			if(p == 0)
				link[i] = fake.getUrl();
			else
				link[i-7] = fake.getUrl();
		}
	}
	for(int i = 0; i < 4; i++){
		int n = i;
		if(i > 0)
			n = n*2;
		else
			n = 1;
		Board ai = (*this);
		int choice;
		int points;
		Algorithm(playerTurn, ai, n, choice, points);
		debug << "\naiChoice: " << choice << "\n";
		if(getContent(choice, "generateLinks") != 0)
			ai.move(choice, "generateLinks");
		link[i+6] = ai.getUrl();
	}
}
int themeVerifier(int n, string theme, const Board &game){
	if(theme == "classic")
		if(game.getContent(n,"themeVerifier") < 10)
			return game.getContent(n,"themeVerifier");
		else
			return 10;
	else
		return game.getContent(n,"draw");
}
void Board::draw(string theme, int turn){
	check();
	int winner;
	bool t = isEnd();
	if(t)
		winner = whoWins();	
	string square = "";
	string tall = "";
	if(theme == "future" || theme == "classic"){
		square = "square";
		tall = "tall";
	}
//	//
	cout << "Content-type:text/html\r\n\r\n";
	cout << "\n<!DOCTYPE html>";
	cout << "\n<LINK REL=StyleSheet HREF='/mancala/" << theme << "/" << theme << ".css' TYPE='text/css' MEDIA=screen>";
	cout << "\n<html lang='en-US'>";
	cout << "\n<head>";
	cout << "\n<title>Mancala in C++</title>";
	cout << "\n</head>";
	cout << "\n<body>";
	cout << "\n<table style='width:960' class=\"background\">";
//	//left sideboard
	cout << "\n<tr valign='middle' height='60'>";
	if(win != " ")
		cout << "\n<th rowspan='4' class=\"reset_tall\"><a href='/cgi-bin/mancala.cgi/" << theme << "'><img src='/mancala/" << theme
		<< "/reset.png' alt='Reset' title='Reset'></td>";
	else{
		cout << "\n<th rowspan='2' width='100' class=\"npc\">";
		cout << "\n<a href='/cgi-bin/mancala.cgi/" << theme << '/' << (turn+1) << link[9] << "'><img src='/mancala/" << theme
		<< "/extreme.png' alt='NPC' title='NPC'></a>";
		cout << "\n<a href='/cgi-bin/mancala.cgi/" << theme << '/' << (turn+1) << link[8] << "'><img src='/mancala/" << theme
		<< "/hard.png' alt='NPC' title='NPC'></a>";
		cout << "\n<a href='/cgi-bin/mancala.cgi/" << theme << '/' << (turn+1) << link[7] << "'><img src='/mancala/" << theme
		<< "/medium.png' alt='NPC' title='NPC'></a>";
		cout << "\n<a href='/cgi-bin/mancala.cgi/" << theme << '/' << (turn+1) << link[6] << "'><img src='/mancala/" << theme
		<< "/easy.png' alt='NPC' title='NPC'></a>";
		cout << "</td>";
	}
//	//top of board	
	if(getPlayer() == 0)
		cout << "\n<td class=\"topbar_valid\">";
	else
		cout << "\n<td class=\"topbar\">";
	cout << "<img src='/mancala/" << theme << "/player1.png' alt='Player 1'></td>";
	cout << "\n<td colspan='6' class=\"topbar\" align='center'<h3>";
	if(win == " ")
		cout << "Player " << getPlayer()+1 << "'s Turn | Turn " << turn << "</h3></td>";
	else
		if(winner == 0)
			cout << win << " " << getScore(0) << " to " << getScore(1) << " in " << turn << " turns";
		else if(winner == 1)
			cout << win << " " << getScore(1) << " to " << getScore(0) << " in " << turn << " turns";
	cout << "</h3></td>";
	if(getPlayer() == 1)
		cout << "\n<td class=\"topbar_valid\">";
	else
		cout << "\n<td class=\"topbar\">";
	cout << "<img src='/mancala/" << theme << "/player2.png' alt='Player 2'></td>";
//	//right sideboard
	if(win != " ")
		cout << "\n<th rowspan='4' class=\"reset_tall\"><a href='/cgi-bin/mancala.cgi/" << theme << "'><img src='/mancala/" << theme
		<< "/reset.png' alt='Reset' title='Reset'></td>";
	else{
		cout << "\n<th rowspan='2' width='100' class=\"npc\">";
		cout << "\n<a href='/cgi-bin/mancala.cgi/" << theme << '/' << (turn+1) << link[9] << "'><img src='/mancala/" << theme
		<< "/extreme.png' alt='NPC' title='NPC'></a>";
		cout << "\n<a href='/cgi-bin/mancala.cgi/" << theme << '/' << (turn+1) << link[8] << "'><img src='/mancala/" << theme
		<< "/hard.png' alt='NPC' title='NPC'></a>";
		cout << "\n<a href='/cgi-bin/mancala.cgi/" << theme << '/' << (turn+1) << link[7] << "'><img src='/mancala/" << theme
		<< "/medium.png' alt='NPC' title='NPC'></a>";
		cout << "\n<a href='/cgi-bin/mancala.cgi/" << theme << '/' << (turn+1) << link[6] << "'><img src='/mancala/" << theme
		<< "/easy.png' alt='NPC' title='NPC'></a>";
		cout << "</td>";
	}
	cout << "\n</tr><tr>";
// 	//pocket 6
	cout << "\n<th rowspan='2' class=\"" << tall << "\"><img id=p6 src='/mancala/" << theme << "/" << themeVerifier(6, theme, (*this)) << ".png' alt='"
	<< themeVerifier(6, theme, (*this)) << "' width='120' height='120' title='Pocket 6, [" << getContent(6,"draw") << "]'></td>";
//	//pockets 5-0	
	for(int i = 5; i >= 0; i--){
		if(getPlayer() == 0 && getContent(i,"draw") > 0)
			cout << "\n<td class=\"valid\"><a href='/cgi-bin/mancala.cgi/" << theme << '/' << (turn+1) << link[i] << "'>";
		else
			cout << "\n<td class=\"" << square << "\">";
		cout << "<img id=p" << i << " src='/mancala/" << theme << "/" << themeVerifier(i, theme, (*this)) << ".png' alt='" << getContent(i,"draw")
		<< "' width='120' height='120' title='Pocket " << i << ", [" << themeVerifier(i, theme, (*this)) << "]'></td></a>";
	}
//	//pocket 13
	cout << "\n<th rowspan='2'  class=\"" << tall << "\"><img id=p13 src='/mancala/" << theme << "/" << themeVerifier(13, theme, (*this)) << ".png' alt='"
	<< themeVerifier(13, theme, (*this)) << "' width='120' height='120' title='Pocket 13, [" << getContent(13,"draw") << "]'></td>";
	cout << "\n</tr>\n<tr>";
//	//left sideboard
	if(win == " ")
		cout << "\n<th rowspan='2' class=\"reset\"><a href='/cgi-bin/mancala.cgi/" << theme << "'><img src='/mancala/"
		<< theme << "/reset.png' alt='Reset' title='Reset'></a></td>";
//	//pockets 7-12
	for(int i = 7; i <= 12; i++){
		if(getPlayer() == 1 && getContent(i,"draw") > 0)
			cout << "\n<td class=\"valid\"><a href='/cgi-bin/mancala.cgi/" << theme << '/' << (turn+1) << link[i-7] << "'>";
		else
			cout << "\n<td class=\"" << square << "\">";
		cout << "<img id=p" << i << " src='/mancala/" << theme << "/" << themeVerifier(i, theme, (*this)) << ".png' alt='" << getContent(i,"draw")
		<< "' width='120' height='120' title='Pocket " << i << ", [" << themeVerifier(i, theme, (*this)) << "]'></td></a>";
	}
//	//right sideboard
	if(win == " ")
		cout << "\n<th rowspan='2' class=\"reset\"><a href='/cgi-bin/mancala.cgi/" << theme << "'><img src='/mancala/"
		<< theme << "/reset.png' alt='Reset' title='Reset'></a></td></tr>";
//	//bottom sideboard
	cout << "\n<tr>\n<th colspan='2' class=\"menu\"><a href='/mancala/'><img src='/mancala/" << theme
	<< "/backtomenu.png' alt='Back to menu' title='Back to menu'></a></td>";
	
	cout << "\n<th colspan='2' class=\"theme\"><a href='http://www.towells.net/cgi-bin/mancala.cgi/retro/" << turn << getUrl()
	<< "'><img src='/mancala/retro/retro.png' alt='Retro' title='Retro'></a>";
	
	cout << "\n<th colspan='2' class=\"theme\"><a href='http://www.towells.net/cgi-bin/mancala.cgi/classic/" << turn << getUrl()
	<< "'><img src='/mancala/classic/classic.png' alt='Classic' title='Classic'></a>";
	
	cout << "\n<th colspan='2' class=\"theme\"><a href='http://www.towells.net/cgi-bin/mancala.cgi/future/" << turn << getUrl()
	<< "'><img src='/mancala/future/future.png' alt='Future' title='Future'></a>";
	
	cout << "</tr>";
	
//	//
	cout << endl << "</table>";
	cout << "\n<div class=\"section group\" align='center'>";
	cout << "\n<div class=\"col span_1_of_4\">";
	cout << "\n</div>";
	cout << "\n<div class=\"col span_2_of_4\" align='center'>";
	cout << "\n<img src='/mancala/index/mancala.png' alt='Mancala' title='Mancala'>";
	cout << "\n</div>";
	cout << "\n</div>";
	cout << endl << "</body>";
	cout << endl << "</html>";
	check();
}

void Algorithm(int p, const Board &ai, int depth, int &choice, int &points){
	if(depth == 0)
		points = ai.getValue(ai.getPlayer());//this is the opponent's perspective of the points, assuming the original player didnt get an extra turn
	else{
		int value = -49;
		int npoints;
		int trash = -1;
		int nchoice = 7*ai.getPlayer();
		Board copy;
		for(int i = 7*ai.getPlayer(); i < 7*(ai.getPlayer()+1)-1; i++){
			copy = ai;
			if(copy.getContent(i, "algorithm") > 0){
				assert(i != -1);
				assert(i != 14);
				copy.move(i, "Algorithm");
				Algorithm(copy.getPlayer(), copy, depth-1, trash, npoints);
				if(p != copy.getPlayer())
					npoints *= -1;
				if(npoints > value){
					value = npoints;
					nchoice = i;
				}
			}
		}
		if(trash == -1)
			Algorithm(copy.getPlayer(), copy, 0, trash, value);
		else
			if(ai.getPlayer() == 0)
				assert(nchoice > -1 && nchoice < 6);
			else if(ai.getPlayer() == 1)
				assert(nchoice > 6 && nchoice < 13);
		choice = nchoice;
		points = value;
	}
}

//input, game procedure
int main(){
	Board game;
	debug << "Debug report : ";
	string theme = "classic";
	int turn = 1;
	int points = -49;
	int player = 0;
	
	string URL = getenv("REQUEST_URI");
	for(int i = 0; i < URL.length(); i++)
		if(URL[i] == '/')
			URL[i] = ' ';
	
	string dummy;
	stringstream ssURL(URL);
	ssURL >> dummy >> dummy >> theme >> turn >> player;
	
	if(turn > 1)
		game.unpause(URL);
	debug << "\nmainplayer: " << player << "\n";
	game.generateLinks(turn, theme);
	debug << "turn: " << turn << " ";
	game.draw(theme, turn);	
}